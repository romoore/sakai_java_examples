package edu.rutgers.sakai.java.generics;

import java.util.LinkedList;
import java.util.List;

/**
 * A simple generic class that encapsulates two objects. The objects may be of
 * any type (or the same type), and are referred to as "first" and "second".
 * 
 * @author Robert Moore
 * @param <T1> any type, referenced via "first".
 * @param <T2> any type, referenced via "second".
 */
public class Pair<T1, T2> {

	/**
	 * The first element of this pair.
	 */
	protected T1 first = null;

	/**
	 * The second element of this pair.
	 */
	protected T2 second = null;

	/**
	 * Creates a new {@code Pair} with a pair of {@code null} values.
	 */
	public Pair() {
	}

	/**
	 * Creates a new {@code Pair} with the specified {@code first} and
	 * {@code second} values.
	 * 
	 * @param first
	 *            the value of the first element of the pair.
	 * @param second
	 *            the value of the second element of the pair.
	 */
	public Pair(T1 first, T2 second) {
		this.first = first;
		this.second = second;
	}

	/**
	 * Gets the first element of this pair.
	 * 
	 * @return the first element of this pair.
	 */
	public T1 getFirst() {
		return this.first;
	}

	/**
	 * Sets the first element of this pair.
	 * 
	 * @param first
	 *            the new first element of this pair.
	 */
	public void setFirst(T1 first) {
		this.first = first;
	}

	/**
	 * Gets the second element of this pair.
	 * 
	 * @return the second element of this pair.
	 */
	public T2 getSecond() {
		return this.second;
	}

	/**
	 * Sets the second element of this pair.
	 * 
	 * @param second
	 *            the new second element of this pair.
	 */
	public void setSecond(T2 second) {
		this.second = second;
	}

	/**
	 * Provides a simple non-interactive sample of how to use this class.
	 * 
	 * @param args
	 *            Ignored.
	 */
	public static void main(String[] args) {
		// Create a new Pair that uses a String and an Integer
		Pair<String, Integer> howManyCats = new Pair<String, Integer>();

		howManyCats.setFirst("cats");
		howManyCats.setSecond(Integer.valueOf(10));

		System.out.println("There are " + howManyCats.getSecond() + " "
				+ howManyCats.getFirst() + " available.");

		// Just a sysout separator
		System.out.println("\n---------\n");

		// Create a new list of Pairs of String/Integer objects
		List<Pair<String, Integer>> inventory = new LinkedList<Pair<String, Integer>>();
		// Add 3 examples of String/Integer pairs
		inventory.add(new Pair<String, Integer>("dogs", Integer.valueOf(5)));
		inventory.add(howManyCats);
		inventory.add(new Pair<String, Integer>("birds", Integer.valueOf(12)));

		// Go through and print each one
		for (Pair<String, Integer> item : inventory) {
			System.out.println("There are " + item.getSecond() + " "
					+ item.getFirst() + " available.");
		}

		// Just a sysout separator
		System.out.println("\n---------\n");

		// Now let's use the convenience class we created below...
		List<InventoryItem> coolInventory = new LinkedList<InventoryItem>();
		InventoryItem someItem = new InventoryItem("something", 5);
		coolInventory.add(someItem);
		coolInventory.add(new InventoryItem("sandwich", 3));

		InventoryItem changed = new InventoryItem(null, 0);
		changed.setName("pencil");
		changed.setQuantity(7);
		coolInventory.add(changed);

		for (InventoryItem item : coolInventory) {
			System.out.println("There are " + item.getQuantity() + " units of "
					+ item.getName() + " available.");
		}
	}

	/**
	 * For convenience, we've created a second class that extends Pair
	 * specifically for our inventory items. Each inventory item will contain a
	 * String, called name, and an Integer, called quantity.
	 * 
	 * @author Robert Moore
	 * 
	 */
	public static class InventoryItem extends Pair<String, Integer> {

		/**
		 * Creates a new Inventory item with the specified name and initial quantity.
		 * @param name the name of this inventory item.
		 * @param quantity the initial quantity of this item in inventory.
		 */
		public InventoryItem(String name, int quantity) {
			super.first = name;
			super.second = Integer.valueOf(quantity);
		}

		/**
		 * Sets the item's name.
		 * 
		 * @param itemName
		 *            the new name of the item.
		 */
		public void setName(String itemName) {
			super.first = itemName;
		}

		/**
		 * Gets the item's name.
		 * 
		 * @return the name of the item.
		 */
		public String getName() {
			return super.first;
		}

		/**
		 * Sets the item's quantity.
		 * 
		 * @param itemQuantity
		 *            the new quantity of the item.
		 */
		public void setQuantity(int itemQuantity) {
			super.second = Integer.valueOf(itemQuantity);
		}

		/**
		 * Gets the item's quantity.
		 * 
		 * @return the quantity of the item.
		 */
		public int getQuantity() {
			return super.second.intValue();
		}
	}
}
