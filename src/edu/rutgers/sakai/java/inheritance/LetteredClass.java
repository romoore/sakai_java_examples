package edu.rutgers.sakai.java.inheritance;

/**
 * A simple class that keeps a char value. {@code LetteredClass} is 
 * a descendant of {@code BasicClass}.  It overrides the {@code tostring()} method
 * to output the char it was created with.
 * @author Robert Moore
 *
 */
public class LetteredClass extends BasicClass {
	
	/**
	 * Creates a new {@code LetteredClass} object with a specific char value.
	 * @param letter the char to use for this object.
	 */
	public LetteredClass(final Character letter){
		super(letter);
	}
	
	/**
	 * Returns the string "Lettered (?)", where ? is the char the object was created with.
	 * Overrides {@link BasicClass#toString()}.
	 */
	@Override
	public String toString(){
		return "Lettered (" + this.value.toString() + ")";
	}
	
	/**
	 * Returns a string that is unique to {@code LetteredClass}. This method cannot be overridden
	 * by descendants because it is a {@code final} method.
	 * @return the string "Unique to Lettered".
	 */
	public final String descendantOnly(){
		return "Unique to Lettered";
	}
}
