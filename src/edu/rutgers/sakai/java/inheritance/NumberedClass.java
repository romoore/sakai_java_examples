package edu.rutgers.sakai.java.inheritance;

/**
 * A simple class that keeps an integer value.  {@code NumberedClass} is 
 * a descendant of {@code BasicClass}.  It overrides the {@code tostring()} method
 * to output the number it was created with.
 * @author Robert Moore
 *
 */
public class NumberedClass extends BasicClass {
	
	/**
	 * Creates a new {@code NumberedClass} object with a specific integer value.
	 * @param number the number to use for this object.
	 */
	public NumberedClass(final Integer number){
		super(number);
	}
	
	/**
	 * Returns the string "Numbered (#)", where # is the number the object was created with.
	 * Overrides {@link BasicClass#toString()}.
	 */
	@Override
	public String toString(){
		return "Numbered (" + this.value.toString() + ")";
	}
	
	/**
	 * Returns a string that is unique to {@code NumberedClass}. This method cannot be overridden
	 * by descendants because it is a {@code final} method.
	 * @return the string "Unique to Numbered".
	 */
	public final String descendantOnly(){
		return "Unique to Numbered";
	}
}
