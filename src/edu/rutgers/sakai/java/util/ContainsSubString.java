package edu.rutgers.sakai.java.util;

/**
 * Shows to to match a substring using labels in Java.
 * 
 * @author Robert Moore
 * 
 */
public class ContainsSubString {

	/**
	 * Determines whether {@code sub} is a substring of {@code complete},
	 * demonstrating the use of labels in Java.
	 * 
	 * @param complete
	 *            a String.
	 * @param sub
	 *            another String.
	 * @return {@code true} if {@code sub} is a substring of {@code complete},
	 *         else false.
	 */
	public static boolean containsSubstring(final String complete,
			final String sub) {
		outer: for (int i = 0; i < complete.length(); ++i) {
			for (int j = 0; j < sub.length(); ++j) {
				if (j + i >= complete.length()) {
					break outer;
				}
				if (sub.charAt(j) != complete.charAt(i + j)) {
					// Continue outer, because break would cause a return of
					// true
					continue outer;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Test method for the containsSubstring method.
	 * @param args ignored.
	 */
	public static void main(String[] args) {
		String biggerOne = "12345abcdefg6789";
		String notASubstring = "67890";
		String substring = "efg";

		// False
		System.out.println("\"" + biggerOne + "\" contains \"" + notASubstring
				+ "\"? " + containsSubstring(biggerOne, notASubstring));

		// True
		System.out.println("\"" + biggerOne + "\" contains \"" + substring
				+ "\"? " + containsSubstring(biggerOne, substring));

	}
}
